
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Index.vue') },
      // { path: '/beranda', name: 'beranda', component: () => import('pages/index') },
      { path: '/content/:id', name: 'content', component: () => import('pages/content.vue') },
      { path: '/content_produk/:id', name: 'content_produk', component: () => import('pages/content_produk.vue') },
      { path: '/content_direktori', name: 'content_direktori', component: () => import('pages/content_direktori.vue') },
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
